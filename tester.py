"""Simple Fume sensor with the CCS811 + BME280 sensor, visualized with a 0.91 Oled screen and 4 Neopixels"""
from machine import Pin, I2C
import time
import CCS811, BME280
import ssd1306
from neopixel import NeoPixel

# i2c bus with scl = D1 (GPIO5) and sda= D2 (GPIO4)
i2c = I2C(scl=Pin(5), sda=Pin(4))
# CCS811 sensor  = s
s = CCS811.CCS811(i2c)
# BME280 sensor = b (including Temp, Hum, Pressure(?) )
b = BME280.BME280(i2c=i2c)
# Oled screen at 0x3c on i2x bus (128x64 pixels)
d = ssd1306.SSD1306_I2C(128,64,i2c,0x3c)
# Neopixel output on D4 (GPIO0) with 4 lights
numberOfPixels = 4
pin = Pin(0, Pin.OUT)
np = NeoPixel(pin, numberOfPixels)
#smoothing data
a = 0.10                  # Weighting factor. Lower value is smoother.
previousTemp = 0

TVOC = [0,0,0]

def main():
    #time.sleep(1)
    while True:
        if s.data_ready():

            r = b.read_compensated_data()
            global t
            t = r[0]/100
            p = r[1]/25600
            h = r[2]/1024
            x,y = s.get_baseline()
            s.put_envdata(humidity=h,temp=t)
            TVOC.insert(0,s.tVOC)
            data()
            update_screen()
            update_lights()
            #store_value()              #if you want to log Data to file

def data():
    if len(TVOC) > 100:
        TVOC.pop(100)
    global upper
    upper = max(TVOC)
    global average
    average = sum(TVOC)/len(TVOC)
    #print(TVOC[0], average, upper)
    #print(TVOC)
    #print('eCO2: %d ppm, TVOC: %d ppb' % (s.eCO2, s.tVOC), b.values)
    #TVOC.insert(0,EWMF)
    ### deleting outliers ###
    if int(TVOC[2]) >= (2*int(TVOC[3])) and int(TVOC[2]) >= (2*int(TVOC[1])) and int(TVOC[2]) != 0 :
        print("big change")
        TVOC.pop(2)


############ showing information on the screen ############################
def update_screen():
    d.fill(0)
    for i in range(len(TVOC)):
        j = int(translate(TVOC[i], 0,upper,50,0))
        for x in range(50, j, -1):
            d.pixel(i, x, 1)
    for y in range(50, 0, -1):
            d.pixel(100, y, 1)
    for z in range(100, 0, -1):
            d.pixel(z, 50, 1)
    d.text(str(upper),105,0)
    d.text(str(TVOC[0]),0,55)
    d.text(str(int(average)),105,25)
    d.text(str(0),105,50)
    #TODO ----> show temp & humitidit
    d.show()

############ showing information on the screen ############################
def translate(value, leftMin, leftMax, rightMin, rightMax):
    # Figure out how 'wide' each range is
    leftSpan = leftMax - leftMin
    rightSpan = rightMax - rightMin
    # Convert the left range into a 0-1 range (float)
    if leftSpan == 0:
        valueScaled = 0
    else:
        valueScaled = float(value - leftMin) / float(leftSpan)
    # Convert the 0-1 range into a value in the right range.
    return rightMin + (valueScaled * rightSpan)

def update_lights():
    if average < 5:
        for i in range(numberOfPixels):
            np[i] = (255,0,0)
    if average > 5:
        for i in range(numberOfPixels):
            np[i] = (0,255,0)
    np.write()




############ uncomment this if you want to store values in a .csv file format ############################
#def store_value():
    #f = open('data.csv', 'a')
    #f.write(str(s.eCO2)+','+str(s.tVOC)+','+str(t)+','+str(p)+'\n')
    #f.close()
main()
