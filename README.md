# Air Quality sensor for workspaces

<img src="img/sensor.gif" width="300">
<img src="img/light.gif" width="300">



A project to give an indication of the amount of fumes in the air and whether that is harmful.

<img src="img/injection.png" width="200">
<img src="img/3Dprinting.png" width="200">
<img src="img/soldering.png" width="200">


The goal of this project is to get more insights in the fumes produces while using melting plastic, either 3D printing or using Precious Plastic machines or when soldering

About 3D printing:
Plastic melting isn't much of a problem, but it depends on the material, duration, size of the room and fresh air facilities. Some more info can be found in the [Documentation](documentation) or [here](http://diy3dtech.com/3d-printing-and-air-quality-risks/)


About precious plastic machines:
> Plastic recycling is not harmful by itself, improper use of machines is.

If plastic is heated up too much the plastic starts to burn and creates fumes that are not healthy. For version 4 there has been a lot of research into what kind of fumes and whether they are harmful or not. like for example [here](https://davehakkens.nl/community/forums/topic/v4-fume-extraction-4/) and [here](https://community.preciousplastic.com/academy/plastic/safety)

This research led to the following information [video](https://www.youtube.com/watch?v=bsj6qHHLynk)



## Now the idea is to make a portable VOC sensor
<img src="img/CurrentSetup.png" width="600">

somethat that could look like this:
https://www.instructables.com/id/Bitcoin-Ticker-With-Graph/

and that uses the css811 sensor:
https://learn.adafruit.com/adafruit-ccs811-air-quality-sensor/overview

for this project I'm using MicroPython, im totally new to this, but it seems great!!


## Bill of materials


| ID	| Name	| Designator	| Footprint	| Quantity |
| ---   | ---   | ---           |  ------   |---------:|
|1	|ccs811	|U4	|Breakout board	|1|
|2	|TMB12A05	|BUZZER1	|B-12*7.5|	1|
|3	|BME280	|P1	|Breakout board	|1|
|4	|WEMOS D1	|U1	|WEMOS D1 Breakout board|	1|
|5	|WS2812B	|LED1,LED2	|LED-SMD_4P-L5.0-W5.0-BL|	2|
|6	|0.96_OLED	|U3	|0.96 OLED|	1|


## Progress
In the following document I write down the progress that I made so far: 👉[link to progress](progress.md) 👈

1.  Week 1: Flashed MicroPython on an esp8266, connected a css811 sensor and 0.96 inch oled display
2.  Week 2: Made a small graph on the display, this shows previous values and scales according to the maximum values of the last 128 measurements
3.  Week 3: Had some overlap in text on the display, made some preperation for the BMP280
4.  Week 4: Added a BMP280 sensor, for temperature calibration, added some links to the readme.
5.  Week 5: Swapped to BME280 sensor and moved to a protoboard
6.  Week 6: Added a case around the pcb
7.  Week 7: Made everything into a small PCB 
8.  Week 8: 0.8mm mat black PCBs arrives, and boy they are gorgous!




### To Do list:
- [x] Prototype
- [x] Storage of values
- [x] Indicator (sound? light)
- [ ] Buzzer implemented in code
- [ ] Calibration
- [x] Housing
- [ ] Building instruction
- [ ] Feedback from community
- [x] PCB